<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\LoginController;
use Crypt;
use Google2FA;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use ParagonIE\ConstantTime\Base32;

class Google2FAController extends Controller
{

    use RedirectsUsers;

    /**
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function enableTwoFactor(Request $request)
    {
        //generate new secret
        $secret = $this->generateSecret();

        //get user
        $user = $request->user();

        // save secret
        $user->google2fa_secret = $secret;
        $user->save();

        //generate image for QR barcode
        $imageDataUri = Google2FA::getQRCodeInline(
            $request->getHttpHost(),
            $user->email,
            $secret,
            200
        );

        return view('2fa/enableTwoFactor', ['image' => $imageDataUri,
                                            'secret' => $secret]);

    }

    /**
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function disableTwoFactor(Request $request)
    {
        $user = $request->user();

        //make secret column blank
        $user->google2fa_secret = null;
        $user->save();

        return view('2fa/disableTwoFactor');
    }

    /**
     * Generate a secret key in Base32 format
     *
     * @return string
     */
    private function generateSecret()
    {
        return Google2FA::generateSecretKey();
    }



}
