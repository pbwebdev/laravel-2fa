<?php

namespace App\Http\Middleware;

use Closure;
use PragmaRX\Google2FALaravel\Middleware;
use PragmaRX\Google2FALaravel\Support\Auth;
use PragmaRX\Google2FALaravel\Support\Config;

class OptionalGoogle2FA extends Middleware
{

    use Auth, Config;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->getUser() && $this->getUser()->{$this->config('otp_secret_column')}){
            return parent::handle($request,$next);
        }
        return $next($request);
    }
    /**
     * Get the current user.
     *
     * @return mixed
     */
    protected function getUser()
    {
        return $this->getAuth()->user();
    }
}
