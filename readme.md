#Laravel Optional Two Factor Authentication Example
Uses this [google2fa package](https://github.com/antonioribeiro/google2fa-laravel) and extends the middleware class to allow optional 2FA protection so 2FA only applies if the user has added a 2FA secret to their account.
