<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::auth();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor')->middleware(['auth']);
Route::get('/2fa/disable', 'Google2FAController@disableTwoFactor')->middleware(['auth']);
Route::post('/2fa/authenticate', 'Auth\LoginController@authenticate2fa')->middleware(['2fa','throttle:5'])->name('2fa.authenticate');

Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth', 'o2fa']);
